package com.example.mersad.watercontrol;

import android.app.Application;

public class AppController extends Application {

    static AppController mInstance;
    private BluetoothHandler bluetoothHandler;


    public static synchronized AppController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public BluetoothHandler getBluetoothHandler() {
        if (bluetoothHandler == null)
            bluetoothHandler = new BluetoothHandler(getApplicationContext());

        return bluetoothHandler;
    }
}
