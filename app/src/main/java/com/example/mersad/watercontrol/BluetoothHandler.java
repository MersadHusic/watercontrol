package com.example.mersad.watercontrol;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class BluetoothHandler {

    // BTLE state
    public List<BluetoothDevice> devices = new ArrayList<>();
    BluetoothAdapter adapter;
    BluetoothGatt gatt;
    BluetoothGattCharacteristic tx;
    BluetoothGattCharacteristic rx;
    public OnDoneListener onUpdate;

    BluetoothHandler(final Context context) {
        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        adapter = bluetoothManager.getAdapter();
    }

    void sendMessage(String message) {
        sendMessage(message.toCharArray());
        Log.d("LOG", "Message sent: " + message);
    }

    private void sendMessage(char[] message) {
        if (tx == null)
            // Do nothing if there is no device or message to send.
            return;

        char[] chars = new char[message.length + 2];
        chars[0] = 11;
        for (int i = 1; i < message.length + 1; i++)
            chars[i] = message[i - 1];
        chars[message.length + 1] = 12;

        // Update TX characteristic value.  Note the setValue overload that takes a byte array must be used.
        tx.setValue(String.valueOf(chars));
        if (gatt.writeCharacteristic(tx))
            writeLine("Sent: " + String.valueOf(chars));
        else {
            writeLine("Couldn't write TX characteristic!");
            Toast.makeText(AppController.getInstance().getApplicationContext(), "Couldn't write TX characteristic!", Toast.LENGTH_LONG).show();
        }
    }

    void receiveMessage(String message) {
        String[] values = message.split(",");

        if (onUpdate != null)
            if (values[0].equals("PAVG"))
                onUpdate.OnDone2(true);
            else if (values[0].equals("PAVH"))
                onUpdate.OnDone2(false);
            else
                onUpdate.OnDone(values[0] + "°C", values[1] + "%", values[2].equals("1"), values[3].equals("1"), values[4].equals("1"), values[5].equals("1"), values[6].equals("1"));
    }

    private void writeLine(final CharSequence text) {
        Log.d("BLE", text.toString());
    }

    public boolean isConnected() {
        if (gatt == null)
            return false;

        try {
            if (gatt.getDevice() == null)
                return false;
        } catch (NullPointerException e) {
            return false;
        }
        return true;
    }
}
