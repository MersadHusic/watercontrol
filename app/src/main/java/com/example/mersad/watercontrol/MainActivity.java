package com.example.mersad.watercontrol;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener, OnDoneListener {

    BluetoothHandler bluetoothHandler;
    TextView temperture, vlaznost, infoPoruka;
    ImageButton plus, minus, settings, save, otvaranjeKrova, zatvaranjeKrova, zalijevanjeGore, zalijevanjeDole, sijalica;
    Settingsdialog settingsdialog;
    Vibrator vibrator;
    Handler handler = new Handler();
    Runnable mRunnable;
    int pasPressed = 0;

    int delay = 2000;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bluetoothHandler = AppController.getInstance().getBluetoothHandler();
        bluetoothHandler.onUpdate = this;
        settingsdialog = new Settingsdialog(this);
        vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);

        temperture = findViewById(R.id.temperatura);
        vlaznost = findViewById(R.id.vlaga);
        plus = findViewById(R.id.plus);
        minus = findViewById(R.id.minus);
        settings = findViewById(R.id.settings);
        save = findViewById(R.id.save);
        infoPoruka = findViewById(R.id.infoPoruka);
        otvaranjeKrova = findViewById(R.id.otvaranjeKrova);
        zatvaranjeKrova = findViewById(R.id.zatvaranjeKrova);
        zalijevanjeGore = findViewById(R.id.zalijevanjeSaVrha);
        zalijevanjeDole = findViewById(R.id.zalijevanjeSaDna);
        sijalica = findViewById(R.id.sijalica);

        plus.setOnTouchListener(this);
        minus.setOnTouchListener(this);
        settings.setOnTouchListener(this);
        save.setOnTouchListener(this);
        otvaranjeKrova.setOnTouchListener(this);
        zatvaranjeKrova.setOnTouchListener(this);
        zalijevanjeGore.setOnTouchListener(this);
        zalijevanjeDole.setOnTouchListener(this);
        sijalica.setOnTouchListener(this);

        plus.setVisibility(View.INVISIBLE);
        minus.setVisibility(View.INVISIBLE);
        mRunnable = new Runnable() {
            @Override
            public void run() {
                bluetoothHandler.sendMessage("PAV" + pasPressed);
                handler.postDelayed(mRunnable, delay);
            }
        };
        handler.post(mRunnable);
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(mRunnable);
        vibrator.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        handler.post(mRunnable);
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            handler.removeCallbacks(mRunnable);
            delay = 2000;
            switch (v.getId()) {
                case R.id.plus:
                    bluetoothHandler.sendMessage("PAV+");
                    vibrator.vibrate(200);
                    break;
                case R.id.minus:
                    bluetoothHandler.sendMessage("PAV-");
                    vibrator.vibrate(200);
                    break;
                case R.id.settings:
                    settingsdialog.show();
                    vibrator.vibrate(200);
                    break;
                case R.id.save:
                    bluetoothHandler.sendMessage("PAVM");
                    vibrator.vibrate(200);
                    break;
                case R.id.otvaranjeKrova:
                    pasPressed = 1;
                    delay = 300;
                    handler.post(mRunnable);
                    infoPoruka.setText("OTVARANJE PLASTENIKA");
                    vibrator.vibrate(new long[]{0, 100000}, 0);
                    break;
                case R.id.zatvaranjeKrova:
                    pasPressed = 2;
                    delay = 300;
                    handler.post(mRunnable);
                    infoPoruka.setText("ZATVARANJE PLASTENIKA");
                    vibrator.vibrate(new long[]{0, 100000}, 0);
                    break;
                case R.id.zalijevanjeSaVrha:
                    bluetoothHandler.sendMessage("PAV3");
                    infoPoruka.setText("PRSKANJE");
                    vibrator.vibrate(200);
                    break;
                case R.id.zalijevanjeSaDna:
                    bluetoothHandler.sendMessage("PAV4");
                    infoPoruka.setText("ZALIVANJE");
                    vibrator.vibrate(200);
                    break;
                case R.id.sijalica:
                    bluetoothHandler.sendMessage("PAV5");
                    infoPoruka.setText("RASVJETA");
                    vibrator.vibrate(200);
                    break;
            }
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            handler.post(mRunnable);
            pasPressed = 0;
            delay = 2000;
            vibrator.cancel();
            infoPoruka.setText("");
        }
        v.performClick();
        return true;
    }

    @Override
    public void OnDone(String temperature, String vlaznost, boolean otvaranjeKrova, boolean zatvaranjeKrova, boolean prskanje, boolean zalivanje, boolean sijalica) {
        temperture.setText(temperature);
        this.vlaznost.setText(vlaznost);
        if (otvaranjeKrova)
            this.otvaranjeKrova.setColorFilter(getResources().getColor(R.color.selected), PorterDuff.Mode.MULTIPLY);
        else
            this.otvaranjeKrova.setColorFilter(null);

        if (zatvaranjeKrova)
            this.zatvaranjeKrova.setColorFilter(getResources().getColor(R.color.selected), PorterDuff.Mode.MULTIPLY);
        else
            this.zatvaranjeKrova.setColorFilter(null);

        if (prskanje)
            this.zalijevanjeGore.setColorFilter(getResources().getColor(R.color.selected), PorterDuff.Mode.MULTIPLY);
        else
            this.zalijevanjeGore.setColorFilter(null);

        if (zalivanje)
            this.zalijevanjeDole.setColorFilter(getResources().getColor(R.color.selected), PorterDuff.Mode.MULTIPLY);
        else
            this.zalijevanjeDole.setColorFilter(null);

        if (sijalica)
            this.sijalica.setColorFilter(getResources().getColor(R.color.selected), PorterDuff.Mode.MULTIPLY);
        else
            this.sijalica.setColorFilter(null);
    }

    @Override
    public void OnDone2(boolean temperature) {
        settingsdialog.dismiss();
        plus.setVisibility(View.VISIBLE);
        minus.setVisibility(View.VISIBLE);
        if (temperature) {
            this.temperture.setBackgroundColor(getResources().getColor(R.color.selected));
            this.vlaznost.setBackgroundColor(Color.WHITE);
        } else {
            this.temperture.setBackgroundColor(Color.WHITE);
            this.vlaznost.setBackgroundColor(getResources().getColor(R.color.selected));
        }
    }
}
