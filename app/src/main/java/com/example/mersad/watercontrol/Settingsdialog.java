package com.example.mersad.watercontrol;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.View;

public class Settingsdialog extends AlertDialog implements View.OnTouchListener {

    private BluetoothHandler bluetoothHandler = new BluetoothHandler(getContext());
    private Vibrator vibrator;

    protected Settingsdialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_dialog);
        setCancelable(true);

        findViewById(R.id.sacuvajTemperatura).setOnTouchListener(this);
        findViewById(R.id.sacuvajVlaznost).setOnTouchListener(this);

        vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            switch (v.getId()) {
                case R.id.sacuvajTemperatura:
                    bluetoothHandler.sendMessage("PAVG");
                    vibrator.vibrate(200);
                    break;
                case R.id.sacuvajVlaznost:
                    bluetoothHandler.sendMessage("PAVH");
                    vibrator.vibrate(200);
                    break;
            }
            v.performClick();
            return true;
        }
        return false;
    }
}
